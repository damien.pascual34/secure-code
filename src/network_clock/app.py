import atexit
import cmd
import logging

import utils.logger
from controllers.client import ClientCMD
from controllers.config import ConfigCMD
from controllers.local import LocalCMD
from utils.exceptions import BreakLoopError


class NetworkClockCMD(cmd.Cmd):
    """Shell for the network clock."""

    prompt = ">>> "
    intro = "Welcome to the network clock shell. Type help or ? to list commands.\n" \
        "Type 'local' to start the local clock shell.\n" \
        "Type 'client' to start the client clock shell.\n" \
        "Type 'config' to start the configuration shell.\n" \

    def __init__(self):
        super().__init__()

        logging.info("Starting network clock shell.")

        self.config_cmd = ConfigCMD()
        self.local_cmd = LocalCMD(self.config_cmd)
        self.client_cmd = ClientCMD(self.config_cmd)

    def emptyline(self) -> bool:
        pass

    def do_local(self, *_):
        """Syntax: local
        Starts interactive shell for local clock.
        """
        self.local_cmd.cmdloop()

    def do_client(self, *_):
        """Syntax: client
        Starts interactive shell for client clock.
        """
        try:
            self.client_cmd.cmdloop()

        except BreakLoopError:
            self.stdout.write("Connection failed.\n")
        
        except Exception as e:
            self.stdout.write(f"Error\n")
            logging.error(f"Error on client: {e}")

    def do_config(self, *_):
        """Syntax: config
        Starts interactive shell for configuration.
        """
        self.config_cmd.cmdloop()

    def do_exit(self, *_):
        """Syntax: exit
        Exits the network clock shell.
        """
        logging.info("Exiting network clock shell.")
        return True


def close(nc: NetworkClockCMD):
    if nc.local_cmd.server:
        nc.local_cmd.server.shutdown()

    if nc.client_cmd.socket:
        nc.client_cmd.socket.close()


def main():
    nc = NetworkClockCMD()
    atexit.register(close, nc)
    nc.cmdloop()
