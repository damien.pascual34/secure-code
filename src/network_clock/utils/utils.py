import datetime
import pathlib
import platform
import re
import subprocess
import logging

RE_IP = r'^([0-9]{1,3}\.){3}[0-9]{1,3}$'
RE_PARSE_LINE = r'(?:"([^"]*)")|(\d+\.\d+|\d+)|(\S+)'


TRUE_VALUES = ("true", "yes", "1", "on", "y")
FALSE_VALUES = ("false", "no", "0", "off", "n")


set_time_path = pathlib.Path(__file__).parent.parent / "resources"


def line_parse(line):
    print(line)
    # Cleaning up the list
    return [arg.group().strip('"') for arg in re.finditer(RE_PARSE_LINE, line)]


def check_port(port) -> bool:
    """Check if port is valid."""
    if not port:
        return False

    try:
        port = int(port)

    except ValueError:
        return False

    return 0 <= port <= 65535


def check_ip(ip) -> bool:
    """Check if ip is valid."""
    if not ip:
        return False

    if ip.lower() == "localhost":
        return True

    return bool(re.match(RE_IP, ip))


def check_formats(formats):
    """Check date and time formats."""
    # TODO: Implement
    return True


def check_config(config) -> bool:
    """Check if config is valid."""
    match config:
        case {
            "ip": str(),
            "port": int(),
            "formats": list(),
            "autostart_server": bool(),
        }:
            return (
                check_ip(config["ip"])
                and check_port(config["port"])
                and check_formats(config["formats"])
            )

        case _:
            return False
    
    
def set_time(dt: datetime.datetime):
    """Set the system time."""
    
    if not isinstance(dt, datetime.datetime):
        raise TypeError("dt must be a datetime.datetime object")
    
    seconds = dt.timestamp()
    
    if platform.system() not in ("Windows", "Linux"):
        raise NotImplementedError("Unsupported platform")
    
    filename = "set_time" + (".exe" if platform.system() == "Windows" else "")
    file = (set_time_path / filename).as_posix()
    
    result = subprocess.run([file, str(seconds)])
    logging.info(result)

if __name__ == "__main__":
    # print(line_parse('auto "2021-01-01 12:00:00"'))
    set_time(datetime.datetime.fromtimestamp(1672531200))
    
