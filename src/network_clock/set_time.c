#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#include <windows.h>

// Define the ARRAYSIZE macro if it's not available
#ifndef ARRAYSIZE
#define ARRAYSIZE(array) (sizeof(array) / sizeof(array[0]))
#endif

BOOL IsRunAsAdministrator()
{
    BOOL fIsRunAsAdmin = FALSE;
    SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
    PSID AdministratorsGroup;

    if (AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
                                 0, 0, 0, 0, 0, 0, &AdministratorsGroup))
    {
        if (CheckTokenMembership(NULL, AdministratorsGroup, &fIsRunAsAdmin))
        {
            fIsRunAsAdmin = fIsRunAsAdmin ? TRUE : FALSE;
        }

        FreeSid(AdministratorsGroup);
    }

    return fIsRunAsAdmin;
}

int setSystemTimeWindows(long timestamp_seconds)
{
    if (!IsRunAsAdministrator())
    {
        printf("Error: The program requires administrator privileges.\n");
        return 1;
    }

    // Convert the seconds timestamp to a FILETIME representation
    ULARGE_INTEGER largeInt;
    largeInt.QuadPart = (ULONGLONG)timestamp_seconds * 10000000ULL + 116444736000000000ULL;

    // Convert FILETIME to SYSTEMTIME
    FILETIME fileTime;
    fileTime.dwLowDateTime = largeInt.LowPart;
    fileTime.dwHighDateTime = largeInt.HighPart;

    SYSTEMTIME sysTime;
    if (!FileTimeToSystemTime(&fileTime, &sysTime))
    {
        printf("Error converting timestamp to system time.\n");
        return 4;
    }

    // Set the new system time
    if (!SetSystemTime(&sysTime))
    {
        printf("Error setting system time.\n");
        return 5;
    }

    printf("System time changed successfully.\n");

    return 0;
}

#endif // _WIN32

#ifdef __linux__
#include <stddef.h>
#include <sys/capability.h>
#include <sys/types.h>
#include <sys/time.h>
#include <ctype.h>

int giveUpCapabilities(cap_value_t *accepted_caps, int n)
{
    cap_t all_caps = cap_get_proc();
    if (all_caps == NULL)
        return 1;
    // This seems like excessive return value checking
    if (cap_clear_flag(all_caps, CAP_PERMITTED) == -1)
    {
        cap_free(all_caps);
        return 1;
    }
    if (cap_clear_flag(all_caps, CAP_INHERITABLE) == -1)
    {
        cap_free(all_caps);
        return 1;
    }
    if (cap_clear_flag(all_caps, CAP_EFFECTIVE) == -1)
    {
        cap_free(all_caps);
        return 1;
    }
    if (accepted_caps != NULL)
    {
        if (cap_set_flag(all_caps, CAP_PERMITTED, n, accepted_caps, CAP_SET) == -1)
        {
            cap_free(all_caps);
            return 1;
        }
        if (cap_set_flag(all_caps, CAP_EFFECTIVE, n, accepted_caps, CAP_SET) == -1)
        {
            cap_free(all_caps);
            return 1;
        }
    }

    if (cap_set_proc(all_caps) == -1)
    {
        cap_free(all_caps);
        return 1;
    }
    if (cap_free(all_caps) == -1)
        return 1;
    return 0;
}

int setSystemTimeLinux(long timestamp_seconds)
{
    cap_value_t accepted_caps[1] = {CAP_SYS_TIME};
    if (!giveUpCapabilities(accepted_caps, 0))
    {
        printf("Error: The program requires CAP_SYS_TIME privilege on Linux.\n");
        return 1;
    }

    struct timeval time = {
        .tv_sec = timestamp_seconds,
        .tv_usec = 0};

    if (settimeofday(&time, NULL) == -1)
    {
        printf("Error setting system time.\n");
        return 5;
    }

    printf("System time changed successfully.\n");

    return 0;
}

#endif // __linux__

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s <timestamp_in_seconds>\n", argv[0]);
        return 2;
    }

    long timestamp_seconds;
    if (sscanf(argv[1], "%ld", &timestamp_seconds) != 1 || timestamp_seconds < 0)
    {
        printf("Error: Invalid timestamp. Please provide a non-negative integer value.\n");
        return 3;
    }

#ifdef _WIN32
    setSystemTimeWindows(timestamp_seconds);
#endif // _WIN32

#ifdef __linux__
    setSystemTimeLinux(timestamp_seconds);
#endif // __linux__

    return 0;
}
