import cmd
import logging
import socket

from utils.exceptions import BreakLoopError


class ClientCMD(cmd.Cmd):
    """Client shell for the network clock."""

    intro = "\nWelcome to the client shell. Type help or ? to list commands."
    prompt = "(Client) > "

    def __init__(self, config):
        super().__init__()

        logging.info("Starting client shell.")

        self.config = config
        self.socket = None

    def preloop(self) -> None:
        # Connect to the server
        try:
            self.stdout.write("Connecting to server...\n")
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((self.config.ip, self.config.port))
            logging.info("Connected to server")

        except ConnectionRefusedError:
            logging.error("Connection refused.")
            raise BreakLoopError()

        except TimeoutError:
            logging.error("Connection timed out.")
            raise BreakLoopError()

    def postloop(self) -> None:
        # Close the socket
        self.proceed("quit")
        self.socket.close()

    def emptyline(self):
        # Disable the default behavior of repeating the last command
        return

    def do_get(self, args):
        """Syntax: get [format_number|strptime]
        Get the current date and time in the specified format.
        """
        if args.isdigit() and 0 <= int(args) < len(self.config.formats):
            args = self.config.formats[int(args)]
        
        self.stdout.write(f"{self.proceed('get', args)}\n")
    
    def help_get(self):
        formats = "\n".join(f" {i} - {f}" for i, f in enumerate(self.config.formats))
        self.stdout.write(
            "Syntax: get [format_number|strptime]\n"
            "Get the current date and time in the specified format.\n"
            "Formats:\n"
            f"{formats}\n\n"
        )

    def do_ping(self, *_):
        """Syntax: ping
        Ping the server.
        """
        self.stdout.write(f"{self.proceed('ping')}\n")

    def do_exit(self, *_):
        """Syntax: exit
        Exit the client shell.
        """
        return True

    def proceed(self, cmd, args: str = ""):
        """Send a command to the server and return the response."""
        self.socket.sendall((cmd + " " + args).encode())
        return self.socket.recv(1024).decode()
