try:
    import netifaces as ni
    no_netifaces = False
except ImportError:
    no_netifaces = True


def get_interfaces() -> list[str]:
    """Return a list of all available interfaces.
    An interface is available if it has an ipv4 address.
    """
    if no_netifaces:
        raise ImportError("netifaces is not installed.")
    
    return [i for i in ni.interfaces() if ni.ifaddresses(i).get(ni.AF_INET)]

def get_interface_ip(interface: str) -> str:
    """Return the ip address of the given interface."""
    if no_netifaces:
        raise ImportError("netifaces is not installed.")
    
    return ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
