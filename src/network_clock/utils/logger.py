import logging

import platformdirs

log_path = platformdirs.user_log_path("Clock", False)
log_path.mkdir(parents=True, exist_ok=True)

logging.basicConfig(
    filename=log_path / "network_clock.log",
    level=logging.DEBUG,
)
