# Network Clock Project
The Network Clock project is a console application that allows you to interact with the time on your local machine and also enables time retrieval from a remote session through a TCP server.

## Getting Started

### Prerequisites

Before running the application, ensure you have the following installed:
- Python (3.x recommended)
- GCC compiler (for Windows and Linux)

For Linux, you'll also need `libcap-dev`.

### Installation

To get started, follow these steps:
1. Clone the repository:
```
git clone https://gitlab.com/jean-gmrch/network-clock.git
cd network-clock
```

2. Install the required dependencies:
```
pip install -r requirements.txt
```

## Usage

### Time Retrieval and Modification

To retrieve or modify the time of the local machine, run the following command:
```
python src/network-clock
```

### For re-compilation of set_time.c

To enable time retrieval from a remote session, follow these steps:

**For Windows:**

1. Compile the `set_time.c` file:
```
gcc -o resources\set_time.exe set_time.c
```

**For Linux:**

1. Install `libcap-dev`:
```
sudo apt install libcap-dev
```

2. Compile the `set_time.c` file:
```
gcc -o resources/set_time set_time.c -lcap
```

Now you're all set up! You can proceed to run the application and utilize its functionalities.
