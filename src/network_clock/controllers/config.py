import cmd
import json
import logging
import pathlib
import shutil

import platformdirs
from utils.inet import get_interface_ip, get_interfaces
from utils.utils import check_config, check_ip, check_port, TRUE_VALUES, FALSE_VALUES

config_path = platformdirs.user_data_path("Clock", False)
config_path.mkdir(parents=True, exist_ok=True)

config_file = config_path / "config.json"
default_file = (
    pathlib.Path(__file__).parent.parent / "resources" / "default-config.json"
)

if not config_file.exists():
    config_file.write_text(default_file.read_text())

download_path = platformdirs.user_downloads_path()


class ConfigCMD(cmd.Cmd):
    """Config shell for the network clock."""
    
    intro = "\nWelcome to the config shell. Type help or ? to list commands."
    prompt = "(Config) > "

    def __init__(self):
        super().__init__()

        logging.info("Starting config shell.")

        self._config = None
        self.preloop()

    def _preload(self):
        """Load the config file into the config dict."""
        if not check_config(self._config):
            self.stdout.write("Invalid config file. Closing.\n")
            logging.error("Invalid config file. Closing.")
            raise SystemExit(1)

        self.port = self._config["port"]
        self.ip = self._config["ip"]
        self.formats = self._config["formats"]
        self.autostart_server = self._config["autostart_server"]

    def preloop(self) -> None:
        # Load config file
        self._config = json.load(open(config_file, "r"))
        self._preload()

    def postloop(self) -> None:
        # Dump config file
        json.dump(self._config, open(config_file, "w"))
        self._preload()

    def emptyline(self):
        # Disable the default behavior of repeating the last command
        return

    def do_show(self, *_):
        """Syntax: show
        Show the current config
        """
        self.stdout.write(
            f"Current config file: {config_file}\n"
            f"Port: {self._config['port']}\n"
            f"Ip: {self._config['ip']}\n"
            f"Formats: {', '.join(self._config['formats'])}\n"
            f"Autostart server: {self._config['autostart_server']}\n"
        )

    def do_ip(self, args):
        """Syntax: ip [ip]
        Show the current ip
        Set the ip to use for the server
        """
        if not args:
            self.stdout.write(f"Current ip is {self._config['ip']}.\n")
            return

        if not check_ip(args):
            self.stdout.write(f"Invalid ip: {args}\n")
            return

        self._config["ip"] = args
        self.stdout.write(f"Ip set to {args}.\n")

    def do_port(self, args):
        """Syntax: port [port]
        Show the current port
        Set the port to use for the server
        """
        if not args:
            self.stdout.write(f"Current port is {self._config['port']}.\n")
            return

        if not check_port(args):
            self.stdout.write(f"Invalid port: {args}\n")
            return

        self._config["port"] = int(args)
        self.stdout.write(f"Port set to {args}.\n")

    def do_interface(self, args):
        """Syntax: interfaces [interface]
        List all available interfaces
        Set the interface to use for the server
        """
        try:
            if not args:
                self.stdout.write("List of interfaces available:\n")
                for interface, ipv4 in (
                    (i, get_interface_ip(i)) for i in get_interfaces()
                ):
                    self.stdout.write(f" - {interface : <15}-> {ipv4}\n")

                return

            if args not in get_interfaces():
                self.stdout.write("Interfaces available:\n")
                return

            ip = get_interface_ip(args)

            self.stdout.write(f"Interface {args} has the following addresses: {get_interface_ip(args)}\n")
            if not check_ip(ip):
                self.stdout.write(f"Interface {args} has no valid ip address.\n")
                return

            self._config["ip"] = ip
            self.stdout.write(f"Interface {args} set as default interface.\n")

        except ImportError:
            self.stdout.write("netifaces is not installed. Use 'ip' cmd instead.\n")
            logging.error("netifaces is not installed. Use 'ip' cmd instead.")

    def complete_interfaces(self, text, *_):
        return [
            interface for interface in get_interfaces() if interface.startswith(text)
        ]
    
    def do_autostart_server(self, args):
        """Syntax: autostart_server [on|off]
        Show the current autostart_server value
        Set the autostart_server value
        """
        if not args:
            self.stdout.write(f"Autostart server is {self._config['autostart_server']}.\n")
            return
    
        if args.lower() not in TRUE_VALUES + FALSE_VALUES:
            return self.stdout.write(f"Invalid value: {args}\n")
        
        elif args.lower() in TRUE_VALUES:
            self._config["autostart_server"] = True
        
        elif args.lower() in FALSE_VALUES:
            self._config["autostart_server"] = False
    
    def complete_autostart_server(self, text, *_):
        return [value for value in ["on", "off"] if value.startswith(text)]

    def do_import(self, args):
        """Syntax: import [file]
        Import a config file
        """
        if not args:
            self.stdout.write("No file specified.\n")
            return

        file = pathlib.Path(args)
        if not file.exists():
            self.stdout.write(f"File {file} not found.\n")
            return

    def do_export(self, *_):
        """Syntax: export [file]
        Export the current config to a file located at Downloads folder
        """
        shutil.copy(config_file, download_path / "config.json")
        self.stdout.write(f"Config file exported to {download_path / 'config.json'}\n")

    def do_exit(self, args):
        """Syntax: exit
        Exit the config shell
        """
        return True
