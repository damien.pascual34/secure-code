import datetime
import logging
import socket
import socketserver
import threading


class ThreadingTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    """Server class.
    Add number_of_connections attribute to TCPServer. This attribute is used to check if there are still clients connected.
    """
    
    # daemon_threads = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.number_of_connections = 0

    def get_request(self):
        self.number_of_connections += 1
        return super().get_request()

    def close_request(self, request):
        self.number_of_connections -= 1
        return super().close_request(request)


class RequestHandler(socketserver.BaseRequestHandler):
    """Request handler class."""
    
    request: socket.socket

    def setup(self) -> None:
        logging.info(f"{threading.get_ident()}:Connection established.")

        while True:
            result = self.request.recv(1024).decode()

            cmd, *args = result.strip().split(" ", 1)
            args = args[0] if args else ""

            logging.info(
                f"{threading.get_ident()}:Receive command: {cmd} {args if args else '(no args)'}"
            )

            match cmd:
                case "ping":
                    self.send("pong")

                case "get":
                    now = datetime.datetime.now()
                    if not args:
                        self.send(str(now))
                        continue

                    try:
                        self.send(now.strftime(args))
                    except ValueError:
                        self.send(f"Invalid format: {args}\n")

                case "quit":
                    break

                case _:
                    self.send("Unknown command")

        logging.info(f"{threading.get_ident()}:Connection closed.")

    def send(self, data: str) -> None:
        self.request.sendall(data.encode())
