import cmd
import datetime
import logging
import threading
import dateutil.parser

from .worker import RequestHandler, ThreadingTCPServer
from utils.utils import set_time, line_parse


class LocalCMD(cmd.Cmd):
    """Local shell for the network clock."""

    intro = "\nWelcome to the local shell. Type help or ? to list commands."
    prompt = "(Local) > "

    def __init__(self, config):
        super().__init__()

        logging.info("Starting local shell.")

        self.config = config
        self.server: ThreadingTCPServer = None

    def emptyline(self):
        # Disable the default behavior of repeating the last command
        return

    def preloop(self) -> None:
        # Start the server if autostart_server is True
        if self.config.autostart_server:
            self.do_server("start")

    def do_show_inet(self, *_):
        """Syntax: show_inet
        Show the ip and port of the server.
        """
        self.stdout.write(f"IP: {self.config.ip}\n" f"Port: {self.config.port}\n")

    def do_server(self, args):
        """Syntax: server [start|stop|status]
        Start, stop or show the status of the server.
        """
        match args:
            case "start":
                try:
                    self.server = ThreadingTCPServer(
                        (self.config.ip, self.config.port), RequestHandler
                    )
                    threading.Thread(
                        target=self.server.serve_forever, daemon=True
                    ).start()
                    self.stdout.write(
                        f"Server started on {self.config.ip}:{self.config.port}\n"
                    )
                    logging.info(
                        f"Server started on {self.config.ip}:{self.config.port}"
                    )

                except Exception as e:
                    self.stdout.write(f"Failed to start server. Check ip and port.\n")
                    logging.error(f"Failed to start server: {e}")

            case "stop":
                if not self.server:
                    self.stdout.write("Server is not running.\n")
                    return

                if self.server.number_of_connections:
                    self.stdout.write(
                        "There are still clients connected. Please disconnect them first.\n"
                    )
                    return

                self.server.shutdown()
                self.server.server_close
                self.server = None
                self.stdout.write("Server stopped.\n")
                logging.info("Server stopped.")

            case "status":
                self.stdout.write(
                    "Server is running.\n"
                    if self.server
                    else "Server is not running.\n"
                )

    def complete_server(self, text, *_):
        return [s for s in ["start", "stop", "status"] if s.startswith(text)]

    def do_number_of_clients(self, *_):
        """Syntax: number_of_clients
        Show the number of connected clients.
        """
        if not self.server:
            self.stdout.write("Server is not running.\n")
            return

        self.stdout.write(
            f"Number of connected clients: {self.server.number_of_connections}\n"
        )

    def do_get(self, args):
        """Syntax: get [format_number|strptime]
        Get the current date and time in the specified format.
        """
        now = datetime.datetime.now()
        if not args:
            self.stdout.write(f"Current date and time: {now}\n")
            return

        if args.isdigit() and 0 <= int(args) < len(self.config.formats):
            args = self.config.formats[int(args)]

        try:
            self.stdout.write(f"Current date and time: {now.strftime(args)}\n")
        except ValueError:
            self.stdout.write(f"Invalid format: {args}\n")

    def help_get(self):
        formats = "\n".join(f" {i} - {f}" for i, f in enumerate(self.config.formats))
        self.stdout.write(
            "Syntax: get [format_number|strptime]\n"
            "Get the current date and time in the specified format.\n"
            "Formats:\n"
            f"{formats}\n\n"
        )

    def do_set(self, args: str):
        """Syntax: set [format_number|strptime|auto] [datetime]
        Set the current date and time, using the specified format.
        If the format is auto, the datetime will be parsed.
        """

        if not args:
            self.stdout.write("No arguments given.\n")
            return

        try:
            dt_format, value = line_parse(args)
        except ValueError:
            self.stdout.write("Invalid arguments.\n")
            return

        if dt_format.isdigit() and 0 <= int(dt_format) < len(self.config.formats):
            dt_format = self.config.formats[int(dt_format)]

        if dt_format == "auto":
            dt = dateutil.parser.parse(value)

        else:
            dt = datetime.datetime.strptime(value, dt_format)

        set_time(dt)

    def do_exit(self, args):
        """Syntax: exit
        Exit the shell.
        """
        if self.server:
            self.do_server("stop")
            # self.stdout.write("Server is still running. Please stop it first.\n")
            # return

        return self.server is None
